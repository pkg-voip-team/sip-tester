Source: sip-tester
Section: comm
Priority: optional
Maintainer: Debian VoIP Team <pkg-voip-maintainers@lists.alioth.debian.org>
Uploaders:
 Tzafrir Cohen <tzafrir@debian.org>,
 Victor Seva <vseva@debian.org>,
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 libgsl-dev,
 libncurses-dev,
 libnet1-dev,
 libpcap-dev,
 libsctp-dev [linux-any],
 libwolfssl-dev,
 pkgconf,
Build-Conflicts:
 libssl-dev,
Standards-Version: 4.7.0
Homepage: https://sipp.sourceforge.net/
Vcs-Browser: https://salsa.debian.org/pkg-voip-team/sip-tester
Vcs-Git: https://salsa.debian.org/pkg-voip-team/sip-tester.git

Package: sip-tester
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Performance testing tool for the SIP protocol
 sip-tester is a test tool and traffic generator for the SIP
 protocol. It can be used to test SIP equipment like SIP
 proxies, SIP media servers, etc. and to emulate user agents
 calling a SIP system.
 .
 Its features are:
  * a few basic SipStone user agent scenarios included
  * custom XML scenario files even for complex call flows
  * comprehensive set of real-time statistics
  * TCP and UDP transport
  * dynamically adjustable call rates
  - send RTP traffic
 .
 This software is distributed as SIPp by its authors.
 .
 SIP is the Session Initiation Protocol, a standard signalling
 protocol for initiating, modifying, and terminating Internet
 conferencing, telephony (VoIP - Voice over IP), video, and
 instant messaging.
 .
 This package has been built for distributed pauses with the
 GNU Scientific Libraries and without openssl due licenses
 incompatibilities, so authentication is not supported.
